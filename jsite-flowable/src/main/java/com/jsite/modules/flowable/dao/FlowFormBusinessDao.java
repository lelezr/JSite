/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.flowable.dao;

import com.jsite.common.persistence.CrudDao;
import com.jsite.common.persistence.annotation.MyBatisDao;
import com.jsite.modules.flowable.entity.FlowFormBusiness;

/**
 * 流程表单业务总表DAO接口
 * @author liuruijun
 * @version 2019-04-11
 */
@MyBatisDao
public interface FlowFormBusinessDao extends CrudDao<FlowFormBusiness> {

}