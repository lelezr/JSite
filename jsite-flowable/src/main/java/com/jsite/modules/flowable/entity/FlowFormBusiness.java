/**
 * Copyright &copy; 2017-2019 <a href="https://gitee.com/baseweb/JSite">JSite</a> All rights reserved.
 */
package com.jsite.modules.flowable.entity;

import com.jsite.common.persistence.DataEntity;
import org.hibernate.validator.constraints.Length;

/**
 * 流程表单业务总表Entity
 * @author liuruijun
 * @version 2019-04-11
 */
public class FlowFormBusiness extends DataEntity<FlowFormBusiness> {
	
	private static final long serialVersionUID = 1L;
	private String formId;		// 表单编号
	private String procInsId;		// 流程实例ID
	
	public FlowFormBusiness() {
		super();
	}

	public FlowFormBusiness(String formId){
        super();
        this.formId = formId;
	}

	@Length(min=0, max=64, message="表单编号长度必须介于 0 和 64 之间")
	public String getFormId() {
		return formId;
	}

	public void setFormId(String formId) {
		this.formId = formId;
	}
	
	@Length(min=0, max=255, message="流程实例ID长度必须介于 0 和 255 之间")
	public String getProcInsId() {
		return procInsId;
	}

	public void setProcInsId(String procInsId) {
		this.procInsId = procInsId;
	}
	
}